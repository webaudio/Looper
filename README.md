# webaudio-looper

This component uses Javascript Web Audio API to play a loop based on an internal language (CSV like) that implements some basic music scores features.

The audio libraries are made by 8 seconds mp3 files of an instrument's sounds. The Looper class will control the 'S' and 'R' parts of the ADSR (Atack, Decay, Sustain, Release) filter.

There is also a metronome that starts two bars (or configured value) before the loop.

Instruments:

* Electric bass (Warwick Streamer Rockbass 5 strings)

Features currently supported:

* Chords
* Legato
* Pitch bend
* Vibrato
* Hammer on and pull off (legatos of different notes)
* Ghost notes (of each bass string)
* Harmonic (with a respective audio sample)
* General score tempo
* General score time signature (2/4, 3/4, 4/4, 5/4, 5/8, 6/8 and so on...)

Loops (or grooves) are stored in a text (CSV like) file with .groove extension (or database text) and have this structure:

```
loop definition
	author info
	loop score images
		loop score image
	loop info
		loop video
		loop description
	loop parts
		loop part bars
			bar notes
```
## Installation

**Simple example**

```
gr, 1, ts-4/4, bpm-100, 'Loop (or groove) name'
	au, 1, 'Author name'
	gs
		sp, 'http://www.piano-lessons-made-simple.com/images/C_Major.png'
	gi
		gv, 'https://www.youtube.com/embed/v=vX3U-59in-E'
		gd, 'C Major Scale -> C - D - E - F - G - A - B'
	gp, 1
		br, 1
			o2, c, 1
			o2, d, 1
			o2, e, 1
			o2, f, 1
		br, 2
			o2, g, 1
			o2, a, 1
			o2, b, 1
			o3, c, 1
```

**Understanding the code:**

`gr, 1, ts-4/4, bpm-100, 'Loop (or groove) name'` Groove definition (gr) with id 1, time signature 4/4, tempo 100 bpm and name.

`au, 1, 'Author name'` The Loop author (au) id (1) and name.

`gs` groove score block

`sp` groove score page file

`gi` groove info block

`gv` groove video embed link

`gd` groove description

`gp` groove part id 1 block

`br, 1` bar 1 of part 1

`o2, c, 1` octave 2, C, quarter note

`o2, d, 1` octave 2, D, quarter note

`o2, e, 1` octave 2, E, quarter note

`o2, f, 1` octave 2, F, quarter note

`br, 2` bar 2 of part 2

`o2, g, 1` octave 2, G, quarter note

`o2, a, 1` octave 2, A, quarter note

`o2, b, 1` octave 2, B, quarter note

`o3, c, 1` octave 3, C, quarter note

## Examples

**D Major ascending, descending and both**

`gp, 'full'` -> merge all loop parts

```
gr, 2, gt-study, ts-4/4, bpm-100, 'Major scale - D'
	au, 1, 'Fabricio Rodrigues'
	gp, 1, 'Ascending'
		br, 1
			o2, d, 1
			o2, e, 1
			o2, f#, 1
			o2, g, 1
		br, 2
			o2, a, 1
			o2, b, 1
			o3, c#, 1
			o3, d, 1
	gp, 2, 'Descending'
		br, 1
			o3, d, 1
			o3, c#, 1
			o2, b, 1
			o2, a, 1
		br, 2
			o2, g, 1
			o2, f#, 1
			o2, e, 1
			o2, d, 1
	gp, 'full'
```

**Squib Cakes (Tower of Power)**

`st` -> stacato

`r, 1/4` -> 16h note rest

`.` or `dt` -> dotted note

`ch1`, `ch2`, `ch3` -> chords notes

`li1`, `le1` -> legato init and legato end

```
gr, 3, gt-music, ts-4/4, bpm-110, 'Squib Cakes (Tower of Power)'
	au, 2, 'Rocco Prestia'
	gp, 1
		br, 1
			o1, f, 1/2, st
			r, 1/4
			o2, f, 1/4

			r, 1/4
			o2, eb, 1/4
			o2, f, 1/2, st

			r, 1

			o2, eb, 1/4
			o2, d, 1/4
			o2, c, 1/4
			o2, d, 1/4
		br, 2
			o1, f, 1/2, st
			r, 1/4
			o2, f, 1/4

			r, 1/4
			o2, eb, 1/4
			o2, f, 1/2, st

			r, 1/2
			o3, eb, 1/2, ch1
			o3, a, 1/2, ch1

			o3, eb, 1/4, ch2, li1
			o3, a, 1/4, ch2, li2
			o3, e, 1/2, ., ch3, le1
			o3, bb, 1/2, ., ch3, le2
		br, 3
			o1, f, 1/2, st
			r, 1/4
			o2, f, 1/4

			r, 1/4
			o2, eb, 1/4
			o2, f, 1/2, st

			r, 1

			o2, eb, 1/4
			o2, d, 1/4
			o2, c, 1/4
			o2, d, 1/4
		br, 4
			o1, f, 1/2, st
			r, 1/4
			o2, f, 1/4

			r, 1/4
			o2, eb, 1/4
			o2, f, 1/2, st

			o1, a, 1/4
			o1, a, 1/4
			o1, bb, 1/2

			o1, b, 1/4
			o1, b, 1/4
			o2, c, 1/2
```