const path = require('path');
const webpack = require('webpack');

const resolve = (uri) => {
  return path.join(__dirname, uri);
};

const common = {
  entry: {
    'index': './src/index.ts',
    'index.min': './src/index.ts'
  },
  target: 'node',
  output: {
    path: resolve('/dist'),
    filename: 'index.js',
    libraryTarget: 'umd',
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.js', '.ts'],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.worker.\.ts$/,
        use: ['worker-loader', 'babel-loader'],
        include: resolve('src'),
      }
    ],
  },
};

module.exports = common;
