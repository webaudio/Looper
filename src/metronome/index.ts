import { AudioLib, audioLib, AudioUrl, BaseAudioLib, Tempo, tempoFactory } from '../audioLib';
import { Observable } from '../helper/observable';

export interface Metronome extends BaseAudioLib {
  active: boolean;
  preCount: boolean;
  toggleActive: (value?: boolean) => void;
  init: (initTime?: number) => void;
  stop: () => void;
  getWorker: () => Worker;
}

export const metronome = (worker: Worker): Metronome & Observable & AudioLib => {
  if (!worker) {
    throw new Error('Please provide a worker');
  }

  let nextTick = 0;
  let context: AudioContext | undefined;
  let tempo: Tempo;
  let bufferSource: AudioBufferSourceNode;
  let gainNode: GainNode;
  let currentCommands: any;
  let urls: AudioUrl[];
  let active = false;
  let currentBar = 0;
  let currentBeat = 0;
  let info: any;
  let barInfo = {
    first: 0,
    last: 0,
  };
  let notify: (name: string, data: any) => void;
  let preCount = false;

  const setTempo = (beatsPerminute: number, timeSignatureStr: string) => {
    tempo = tempoFactory(beatsPerminute, timeSignatureStr);
  };

  const tick = () => {
    if (!context) {
      worker.postMessage({ type: 'stop' });
      return;
    }

    while (nextTick < context.currentTime + 0.1) {
      const currentTempo = info.tempos.find(
        (t: any) => t.initAtBar <= (currentBar === barInfo.first - 1 ? currentBar + 1 : currentBar),
      );
      const currentTimeSignature = currentTempo.timeSignature;
      setTempo(currentTempo.value, currentTimeSignature.value);

      const noteDuration = tempo.getBeatDuration();

      bufferSource = context.createBufferSource();
      gainNode = context.createGain();

      if (!active && !preCount) {
        gainNode.gain.value = 0;
      } else {
        gainNode.gain.value = currentCommands.gains?.metronome?.gain ?? 1;
      }

      if (urls[0]?.buffer) {
        bufferSource.buffer = urls[0].buffer;
      }

      bufferSource.connect(gainNode);
      gainNode.connect(context.destination);

      bufferSource.start(nextTick);
      worker.postMessage({
        type: 'schedule',
        time: nextTick - context.currentTime,
        eventName: 'beatChange',
      });

      nextTick += noteDuration;
    }
  };

  const beatChange = () => {
    const beats = tempo.timeSignature.getBeats();

    if (currentBeat < beats) {
      if (currentBeat === beats - 1) {
        preCount = false;
      }
      currentBeat += 1;
    } else {
      currentBar = currentBar === barInfo.last ? barInfo.first : currentBar + 1;
      currentBeat = 1;

      notify('barChange', {
        barNumber: currentBar,
      });
    }
  };

  worker.addEventListener('message', event => {
    switch (event.data.type) {
      case 'beatChange':
        beatChange();
        break;
      default:
        tick();
    }
  });

  const audioFiles: AudioUrl[] = [
    {
      name: 'beat',
      value: '/audio/metronome/block.mp3',
    },
  ];

  const lib = audioLib({ urls: audioFiles });

  const child: Metronome & AudioLib & Observable = {
    ...lib,
    active: false,
    init(initTime: number, commands: any) {
      if (!context) {
        context = this.context as AudioContext;
        urls = this.urls;
      }

      if (context.state === 'suspended') {
        context.resume();
      }

      nextTick = initTime;

      preCount = commands.preCount || this.active;
      currentBar = preCount ? commands.initAtBar - 1 : commands.initAtBar;
      barInfo = {
        first: commands.initAtBar,
        last: commands.endAtBar,
      };

      currentBeat = 0;
      currentCommands = commands;

      const currentTempo = info.tempos.find(
        (t: any) => t.initAtBar <= (currentBar === barInfo.first - 1 ? currentBar + 1 : currentBar),
      );
      const currentTimeSignature = currentTempo.timeSignature;

      setTempo(currentTempo.value, currentTimeSignature.value);

      if (this.notify) {
        notify = this.notify;
        this.notify('endPreCount', {
          barNumber: currentBar,
          nextTick: preCount ? initTime + this.getBarInitTime(2, tempo) : nextTick,
        });
        this.notify('barChange', { barNumber: !preCount ? currentBar : 0 });
      }
      worker.postMessage({ type: 'init' });
    },
    stop() {
      worker.postMessage({ type: 'stop' });
      context = undefined;
    },
    setTempo(beatsPerminute: number, timeSignatureStr: string) {
      tempo = tempoFactory(beatsPerminute, timeSignatureStr);
    },
    setInfo(value: any) {
      info = value;
      barInfo = value.barInfo;
    },
    setCommands(commands) {
      barInfo = {
        first: commands.initAtBar ?? 1,
        last: commands.endAtBar ?? barInfo.last,
      };
      currentCommands = commands;
    },
    getInfo() {
      return info;
    },
    toggleActive(value?: boolean) {
      this.active = value !== undefined ? value : !this.active;
      active = this.active;
    },
    getWorker() {
      return worker;
    },
  };

  return child;
};
