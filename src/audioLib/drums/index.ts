import { Observable } from '@/helper/observable';
import { AudioLib, AudioNote, AudioSingleFileUrl } from '../';
import { singleFileInstrument, SingleFileInstrumentLib } from '../single-file-instrument';

// This Library maps the "Four on the floor" Drum kit
// From Logic Pro
const audioFile: AudioSingleFileUrl = {
  id: 1,
  name: 'Four on the floor',
  value: '/audio/drums/four-on-the-floor/lib.mp3',
  noteTotalDurationInSeconds: 2,
};

const audioNotes: AudioNote[] = [
  { id: 68, index: 0, isNaturalRelease: true, name: 'cowbell' },
  { id: 40, index: 1, isNaturalRelease: true, name: 'crash-ls' },
  { id: 61, index: 2, isNaturalRelease: true, name: 'crash-left' },
  { id: 41, index: 3, isNaturalRelease: true, name: 'crash-rs' },
  { id: 69, index: 4, isNaturalRelease: true, name: 'crash-right' },
  { id: 51, index: 5, isNaturalRelease: true, name: 'hand-claps' },
  { id: 54, index: 6, isNaturalRelease: true, name: 'hh-closed' },
  { id: 45, index: 7, isNaturalRelease: true, name: 'hh-foot-close' },
  { id: 43, index: 8, isNaturalRelease: true, name: 'hh-foot-splash' },
  { id: 58, index: 9, isNaturalRelease: false, name: 'hh-open' },
  { id: 60, index: 10, isNaturalRelease: true, name: 'hi-tom' },
  { id: 47, index: 11, isNaturalRelease: true, name: 'kick' },
  { id: 53, index: 12, isNaturalRelease: true, name: 'low-tom' },
  { id: 57, index: 13, isNaturalRelease: true, name: 'mid-tom' },
  { id: 65, index: 14, isNaturalRelease: true, name: 'ride-bell' },
  { id: 64, index: 15, isNaturalRelease: true, name: 'ride-edge' },
  { id: 71, index: 16, isNaturalRelease: true, name: 'ride-in' },
  { id: 63, index: 17, isNaturalRelease: true, name: 'ride-out' },
  { id: 50, index: 18, isNaturalRelease: true, name: 'snare-center' },
  { id: 46, index: 19, isNaturalRelease: true, name: 'snare-edge' },
  { id: 44, index: 20, isNaturalRelease: true, name: 'snare-rs-edge' },
  { id: 52, index: 21, isNaturalRelease: true, name: 'snare-rs' },
  { id: 49, index: 22, isNaturalRelease: true, name: 'snare-ss' },
  { id: 66, index: 23, isNaturalRelease: true, name: 'tambourine' },
];

export const drumsLib = (name = 'drums'): SingleFileInstrumentLib & Observable & AudioLib => {
  const lib = singleFileInstrument({ name, instrument: 'drums', audioNotes, audioFile, chordGainFactor: 1 });
  const child = {
    ...lib,
  };

  return child;
};
