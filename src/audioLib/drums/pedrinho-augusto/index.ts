import { Observable } from '@/helper/observable';
import { AudioLib, AudioNote, AudioSingleFileUrl } from '../../';
import { singleFileInstrument, SingleFileInstrumentLib } from '../../single-file-instrument';

// This Library maps the "Four on the floor" Drum kit
// From Logic Pro
const audioFile: AudioSingleFileUrl = {
  id: 1,
  name: 'Pedrinho Augusto Drums Lib',
  value: '/audio/drums/pedrinho-augusto/lib.mp3',
  noteTotalDurationInSeconds: 5,
};

const audioNotes: AudioNote[] = [
  { id: 68, index: 0, isNaturalRelease: true, name: 'snare-center' },
  { id: 40, index: 1, isNaturalRelease: true, name: 'snare-center-ghost' },
  { id: 61, index: 2, isNaturalRelease: true, name: 'kick' },
  { id: 41, index: 3, isNaturalRelease: true, name: 'kick-teppich-off' },
  { id: 69, index: 4, isNaturalRelease: true, name: 'hh-closed' },
  { id: 51, index: 5, isNaturalRelease: true, name: 'hh-edge' },
  { id: 54, index: 6, isNaturalRelease: false, name: 'hh-open' },
  { id: 45, index: 7, isNaturalRelease: true, name: 'crash' },
  { id: 43, index: 8, isNaturalRelease: true, name: 'hh-foot-close' },
  { id: 58, index: 9, isNaturalRelease: true, name: 'ride-in' },
  { id: 60, index: 10, isNaturalRelease: true, name: 'ride-ghost' },
  { id: 47, index: 11, isNaturalRelease: true, name: 'ride-bell' },
  { id: 53, index: 12, isNaturalRelease: true, name: 'low-tom' },
  { id: 57, index: 13, isNaturalRelease: true, name: 'low-tom-mid' },
  { id: 65, index: 14, isNaturalRelease: true, name: 'snare-ss' },
  { id: 64, index: 15, isNaturalRelease: true, name: 'snare-ss-ghost' },
  { id: 71, index: 16, isNaturalRelease: true, name: 'hh-edge-mid' },
  { id: 63, index: 17, isNaturalRelease: true, name: 'hh-accent-mid' },
  { id: 50, index: 18, isNaturalRelease: true, name: 'kick-mid' },
  { id: 46, index: 19, isNaturalRelease: true, name: 'kick-mid-light' },
  { id: 44, index: 20, isNaturalRelease: true, name: 'snare-buzz' },
  { id: 52, index: 21, isNaturalRelease: false, name: 'snare-rulo-long' },
  { id: 49, index: 22, isNaturalRelease: true, name: 'ride-in-mid' },
  { id: 66, index: 23, isNaturalRelease: true, name: 'kick-rock' },
  { id: 67, index: 24, isNaturalRelease: true, name: 'kick-rock-light' },
  { id: 68, index: 25, isNaturalRelease: true, name: 'snare-center-rock' },
  { id: 681, index: 25, isNaturalRelease: true, name: 'snare-rs' },
  { id: 69, index: 26, isNaturalRelease: true, name: 'hh-open-rock' },
  { id: 70, index: 27, isNaturalRelease: true, name: 'ride-crash-rock' },
  { id: 71, index: 28, isNaturalRelease: true, name: 'kick-20-light' },
  { id: 72, index: 29, isNaturalRelease: true, name: 'kick-20-medium' },
  { id: 73, index: 30, isNaturalRelease: true, name: 'tom-10-light' },
  { id: 731, index: 30, isNaturalRelease: true, name: 'hi-tom' },
  { id: 74, index: 31, isNaturalRelease: true, name: 'tom-10-medium' },
];

export const drumsPedrinhoAugustoLib = (name = 'drums'): SingleFileInstrumentLib & Observable & AudioLib => {
  const lib = singleFileInstrument({ name, instrument: 'drums', audioNotes, audioFile, chordGainFactor: 1 });
  const child = {
    ...lib,
  };

  return child;
};
