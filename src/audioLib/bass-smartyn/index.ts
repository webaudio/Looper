import { Observable } from '../../helper/observable';
import { AudioLib, AudioSingleFileUrl, AudioNote } from '../';
import { singleFileInstrument, SingleFileInstrumentLib } from '../single-file-instrument';

const audioFile: AudioSingleFileUrl = {
  id: 1,
  name: 'S Martyn - Ponte',
  value: '/audio/smartyn/ponte/lib.mp3',
  noteTotalDurationInSeconds: 8,
};

const audioNotes: AudioNote[] = [
  {
    id: 35,
    index: 0,
    name: 'B0',
    isNaturalRelease: false,
  },
  {
    id: 36,
    index: 1,
    name: 'C1',
    isNaturalRelease: false,
  },
  {
    id: 37,
    index: 2,
    name: 'C#1/Db1',
    isNaturalRelease: false,
  },
  {
    id: 38,
    index: 3,
    name: 'D1',
    isNaturalRelease: false,
  },
  {
    id: 39,
    index: 4,
    name: 'D#1/Eb1',
    isNaturalRelease: false,
  },
  {
    id: 40,
    index: 5,
    name: 'E1',
    isNaturalRelease: false,
  },
  {
    id: 41,
    index: 6,
    name: 'F1',
    isNaturalRelease: false,
  },
  {
    id: 42,
    index: 7,
    name: 'F#1/Gb1',
    isNaturalRelease: false,
  },
  {
    id: 43,
    index: 8,
    name: 'G1',
    isNaturalRelease: false,
  },
  {
    id: 44,
    index: 9,
    name: 'G#1/Ab1',
    isNaturalRelease: false,
  },
  {
    id: 45,
    index: 10,
    name: 'A1',
    isNaturalRelease: false,
  },
  {
    id: 46,
    index: 11,
    name: 'A#1/Bb1',
    isNaturalRelease: false,
  },
  {
    id: 47,
    index: 12,
    name: 'B1',
    isNaturalRelease: false,
  },
  {
    id: 48,
    index: 13,
    name: 'C2',
    isNaturalRelease: false,
  },
  {
    id: 49,
    index: 14,
    name: 'C#2/Db2',
    isNaturalRelease: false,
  },
  {
    id: 50,
    index: 15,
    name: 'D2',
    isNaturalRelease: false,
  },
  {
    id: 51,
    index: 16,
    name: 'D#2/Eb2',
    isNaturalRelease: false,
  },
  {
    id: 52,
    index: 17,
    name: 'E2',
    isNaturalRelease: false,
  },
  {
    id: 53,
    index: 18,
    name: 'F2',
    isNaturalRelease: false,
  },
  {
    id: 54,
    index: 19,
    name: 'F#2/Gb2',
    isNaturalRelease: false,
  },
  {
    id: 55,
    index: 20,
    name: 'G2',
    isNaturalRelease: false,
  },
  {
    id: 56,
    index: 21,
    name: 'G#2/Ab2',
    isNaturalRelease: false,
  },
  {
    id: 57,
    index: 22,
    name: 'A2',
    isNaturalRelease: false,
  },
  {
    id: 58,
    index: 23,
    name: 'A#2/Bb2',
    isNaturalRelease: false,
  },
  {
    id: 59,
    index: 24,
    name: 'B2',
    isNaturalRelease: false,
  },
  {
    id: 60,
    index: 25,
    name: 'C3',
    isNaturalRelease: false,
  },
  {
    id: 61,
    index: 26,
    name: 'C#3/Db3',
    isNaturalRelease: false,
  },
  {
    id: 62,
    index: 27,
    name: 'D3',
    isNaturalRelease: false,
  },
  {
    id: 63,
    index: 28,
    name: 'D#3/Eb3',
    isNaturalRelease: false,
  },
  {
    id: 64,
    index: 29,
    name: 'E3',
    isNaturalRelease: false,
  },
  {
    id: 65,
    index: 30,
    name: 'F3',
    isNaturalRelease: false,
  },
  {
    id: 66,
    index: 31,
    name: 'F#3/Gb3',
    isNaturalRelease: false,
  },
  {
    id: 67,
    index: 32,
    name: 'G3',
    isNaturalRelease: false,
  },
  {
    id: 68,
    index: 33,
    name: 'G#3/Ab3',
    isNaturalRelease: false,
  },
  {
    id: 69,
    index: 34,
    name: 'A3',
    isNaturalRelease: false,
  },
  {
    id: 70,
    index: 35,
    name: 'A#3/Bb3',
    isNaturalRelease: false,
  },
  {
    id: 71,
    index: 36,
    name: 'B3',
    isNaturalRelease: false,
  },
  {
    id: 72,
    index: 37,
    name: 'C4',
    isNaturalRelease: false,
  },
  {
    id: 73,
    index: 38,
    name: 'C#4/Db4',
    isNaturalRelease: false,
  },
  {
    id: 74,
    index: 39,
    name: 'D4',
    isNaturalRelease: false,
  },
  {
    id: 75,
    index: 40,
    name: 'D#4/Eb4',
    isNaturalRelease: false,
  },
  {
    id: 76,
    index: 41,
    name: 'E4',
    isNaturalRelease: false,
  },
  {
    id: 77,
    index: 42,
    name: 'F4',
    isNaturalRelease: false,
  },
  {
    id: 78,
    index: 43,
    name: 'F#4/Gb4',
    isNaturalRelease: false,
  },
  {
    id: 79,
    index: 44,
    name: 'G4',
    isNaturalRelease: false,
  },
  {
    id: 80,
    index: 45,
    name: 'G#4/Ab4',
    isNaturalRelease: false,
  },
  {
    id: 81,
    index: 46,
    name: 'A4',
    isNaturalRelease: false,
  },
  {
    id: 82,
    index: 47,
    name: 'A#4/Bb4',
    isNaturalRelease: false,
  },
  {
    id: 83,
    index: 48,
    name: 'B4',
    isNaturalRelease: false,
  },
  {
    id: 84,
    index: 49,
    name: 'C5',
    isNaturalRelease: false,
  },
  {
    id: 85,
    index: 50,
    name: 'ghost_g',
    isNaturalRelease: false,
  },
  {
    id: 86,
    index: 51,
    name: 'ghost_d',
    isNaturalRelease: false,
  },
  {
    id: 87,
    index: 52,
    name: 'ghost_a',
    isNaturalRelease: false,
  },
  {
    id: 88,
    index: 53,
    name: 'ghost_e',
    isNaturalRelease: false,
  },
  {
    id: 89,
    index: 54,
    name: 'ghost_b',
    isNaturalRelease: false,
  },
];

export const smartynBassLib = (name = 'bass'): SingleFileInstrumentLib & Observable & AudioLib => {
  const lib = singleFileInstrument({ name, instrument: 'bass', audioFile, audioNotes, chordGainFactor: 0.5 });

  return {
    ...lib,
  };
};
