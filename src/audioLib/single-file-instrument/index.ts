import { Observable } from '../../helper/observable';
import { singleFileAudioLib, AudioLib, BaseAudioLib, Tempo, tempoFactory } from '../';

export interface SingleFileInstrumentLib extends BaseAudioLib {
  name: string;
  instrument: string;
  setWorker: (worker: Worker) => void;
  setInfo: (info: any) => void;
  setBars: (bars: any) => void;
  humanizeGains: (pct: number) => void;
  resetGains: () => void;
}

export const singleFileInstrument = ({
  audioFile,
  audioNotes,
  name,
  instrument,
  chordGainFactor,
}): SingleFileInstrumentLib & Observable & AudioLib => {
  let worker: Worker;
  let nextTick = 0;
  let context: AudioContext | undefined;
  let tempo: Tempo;
  let bufferSource: AudioBufferSourceNode;
  const gainNodes: GainNode[] = [];
  let currentCommands: any = undefined;
  let audios = [];
  let parsedLib = null;
  let firstBar = 0;
  let lastBar = 0;
  let info: any;
  let bars = [];
  let originalBars = [];
  let hasLoopedOnce = false;
  let notify: (name: string, data: any) => void;
  const barInfo = {
    index: 0,
    noteIndex: 0,
    first: firstBar,
    last: lastBar,
  };

  const getSoundByName = function(name) {
    const url = audios.find(sound => {
      let names = [];
      if (sound.name.indexOf('#') != -1) {
        names = sound.name.split('/');
      }
      return name === sound.name || (names.length > 0 && (name === names[0] || name === names[1]));
    });

    return url;
  };

  const getSoundById = function(id) {
    const url = audios.find(sound => sound.id === id);

    return url;
  };

  const getSound = function(note) {
    const id = note.toPlay;

    if (isNaN(id)) {
      return getSoundByName(id);
    }

    return getSoundById(id);
  };

  const setTempo = (beatsPerminute: number, timeSignatureStr: string) => {
    tempo = tempoFactory(beatsPerminute, timeSignatureStr);
  };

  const updateBarInfo = () => {
    const currentBar = bars[barInfo.index - 1];

    if (barInfo.noteIndex === currentBar.notes.length - 1) {
      barInfo.noteIndex = 0;

      barInfo.index += 1;

      if (barInfo.index === lastBar + 1) {
        if (firstBar === 1 && lastBar === bars.length) {
          firstBar = bars.filter(b => b.isPreLoop).length + 1;
          notify('firstBarChange', { barNumber: firstBar });
        }
        barInfo.index = firstBar;
        hasLoopedOnce = true;
        barInfo.first = firstBar;
      }
    } else {
      barInfo.noteIndex += 1;
    }
  };

  const applyModifiers = ({ note, bars, barInfo, tempo, bufferSource, currentTime }) => {
    let duration = 0;
    if (note.modifiers.length > 0) {
      note.modifiers.forEach(m => {
        if (m.type === 'legato' || m.type === 'glissando' || m.type === 'pitchBend' || m.type === 'vibrato') {
          const modified = m.detune({
            note,
            bars,
            barInfo,
            tempo,
            source: bufferSource,
            currentTime,
          });
          duration += modified.duration;
        }
      });
    }
    return {
      barInfo,
      duration,
    };
  };

  const scheduleNote = (chordNote = 0, force = false) => {
    const currentTempo = info.tempos.find((t: any) => t.initAtBar <= barInfo.index + 1);

    const currentTimeSignature = currentTempo.timeSignature;
    setTempo(currentTempo.value, currentTimeSignature.value);

    const note = bars[barInfo.index - 1].notes[barInfo.noteIndex];

    let noteDuration = tempo.getNoteDuration(note.figure, note.dot);

    if (note && (force || !(note.legatoStop() || note.glissandoStop() || note.rest))) {
      const sound = getSound(note);
      bufferSource = context.createBufferSource();
      const gainNode = context.createGain();

      let gain = 1;
      gain *= isNaN(note.gain) ? 1 : note.gain;
      gain *= note.chord ? chordGainFactor : 1;
      gain *= (currentCommands && currentCommands.gains?.[name]?.gain) ?? 1;
      gainNode.gain.value = gain;

      if (parsedLib?.buffer) {
        bufferSource.buffer = parsedLib.buffer;
      }

      const modifiers = applyModifiers({
        note,
        bars,
        barInfo,
        tempo,
        bufferSource,
        currentTime: nextTick,
      });

      noteDuration += modifiers.duration;
      noteDuration *= note.staccato ? 0.5 : 1;

      bufferSource.connect(gainNode);
      gainNode.connect(context.destination);

      const release = noteDuration > 0.1 ? noteDuration * 0.05 : noteDuration * 0.5;
      const sustain = noteDuration > 0.1 ? noteDuration * 0.95 : noteDuration * 0.5;
      const sustainPedal = note.sustainPedal ? sustain * 0.05 : 0;

      if (!sound.isNaturalRelease) {
        gainNode.gain.setTargetAtTime(0, nextTick + sustain + sustainPedal, release);
      }
      bufferSource.start(
        nextTick,
        sound.index * audioFile.noteTotalDurationInSeconds,
        audioFile.noteTotalDurationInSeconds,
      );
      gainNodes[chordNote] = gainNode;
    } else if (
      note &&
      !note.rest &&
      ((note.legatoStart() && note.glissandoStop()) || (note.glissandoStart() && note.legatoStop()))
    ) {
      const modifiers = applyModifiers({
        note,
        bars,
        barInfo,
        tempo,
        bufferSource,
        currentTime: nextTick,
      });

      noteDuration += modifiers.duration;
      noteDuration *= note.staccato ? 0.5 : 1;
      const gNode = gainNodes[chordNote];

      gNode.gain.cancelScheduledValues(context.currentTime);
      const release = noteDuration > 0.1 ? noteDuration * 0.05 : noteDuration * 0.5;
      const sustain = noteDuration > 0.1 ? noteDuration * 0.95 : noteDuration * 0.5;
      gNode.gain.setTargetAtTime(0, nextTick + sustain, release);
    } else if (
      // Play the note when there is a Legato between last and first note of the loop
      // Also consider if the note is the first note of the first non preLoop bar
      // And the loop should be running for the first time
      // Dont schedule the note if the previous bar is a preLoop and
      // there is a legato start id on the previous bar with the same legato end id of the first note
      note.legatoStop() &&
      !hasLoopedOnce &&
      barInfo.noteIndex === 0 &&
      (barInfo.index === firstBar ||
        (barInfo.index === bars.filter(b => b.isPreLoop).length + 1 &&
          !bars[barInfo.index - 1].notes.find(n => n.legatoStart && n.legatoStart() === note.legatoStop())))
    ) {
      scheduleNote(chordNote, true);
    }
    const nextNote = bars[barInfo.index - 1].notes[barInfo.noteIndex + 1];
    if (!!note.chord && nextNote && !!nextNote.chord && nextNote.chord === note.chord) {
      barInfo.noteIndex += 1;
      scheduleNote(chordNote + 1);
    }
  };

  const tick = () => {
    if (!context) {
      worker.postMessage({ type: 'stop' });
      return;
    }

    while (nextTick < context.currentTime + 0.1) {
      scheduleNote();
      const note = bars[barInfo.index - 1].notes[barInfo.noteIndex];
      const noteDuration = tempo.getNoteDuration(note.figure, note.dot);
      nextTick += noteDuration;
      updateBarInfo();
    }
  };

  const workerMessageListener = event => {
    switch (event.data.type) {
      default:
        tick();
    }
  };

  const lib = singleFileAudioLib({ audioFile, audioNotes });

  const child = {
    ...lib,
    name,
    instrument,
    init(initTime: number, commands: any) {
      if (!context || context.state === 'closed') {
        context = this.context as AudioContext;
        audios = this.audioNotes;
        parsedLib = this.audioFile;
      }

      if (context.state === 'suspended') {
        context.resume();
      }

      if (this.notify) {
        notify = this.notify;
      }

      currentCommands = commands;

      firstBar = commands.initAtBar;
      lastBar = commands.endAtBar;

      barInfo.index = firstBar;
      barInfo.noteIndex = 0;

      nextTick = initTime;
    },
    setWorker(value: Worker) {
      if (worker) {
        worker.removeEventListener('message', workerMessageListener);
      }
      worker = value;
      if (!worker) {
        return;
      }
      worker.addEventListener('message', workerMessageListener);
    },
    setInfo(value: any) {
      info = value;
    },
    setBars(value: any) {
      bars = value.map(bar => {
        bar.notes = bar.notes.map(note => {
          return {
            ...note,
            gain: note.gain ?? 1,
          };
        });

        return bar;
      });
      originalBars = value.map(bar => ({
        ...bar,
        notes: bar.notes.map(note => ({
          ...note,
          gain: note.gain ?? 1,
        })),
      }));
    },
    /**
     *
     * @param pct - number between 0 and 100
     */
    humanizeGains(pct = 15) {
      const newBars = originalBars.map(bar => {
        return {
          ...bar,
          notes: bar.notes.map(note => {
            const min = -pct;
            const max = pct;
            const randomGain = (Math.random() * (max - min) + min) / 100;

            return {
              ...note,
              gain: note.gain + note.gain * randomGain,
            };
          }),
        };
      });

      bars = newBars.map(bar => ({
        ...bar,
        notes: bar.notes.map(note => ({
          ...note,
          gain: note.gain ?? 1,
        })),
      }));
    },
    resetGains() {
      bars = originalBars.map(bar => ({
        ...bar,
        notes: bar.notes.map(note => ({
          ...note,
          gain: note.gain ?? 1,
        })),
      }));
    },
    getInfo() {
      return {
        ...info,
        barInfo,
      };
    },
  };

  return child;
};
