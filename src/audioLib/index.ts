import { observable } from '../helper/observable';

export interface BaseAudioLib {
  context?: AudioContext;
  urls: AudioUrl[];
  audioFile?: AudioSingleFileUrl;
  audioNotes?: AudioNote[];
  init?: (initTime?: number, commands?: any) => void;
  stop?: () => void;
  setInfo: (value: any) => void;
  setTempo: (beatsPerminute: number, timeSignatureStr: string) => void;
  getBarInitTime: (barNumber: number, tempo: number) => number;
}

export interface AudioUrl {
  id?: number | string;
  name: string;
  value: string;
  isNaturalRelease?: boolean;
  buffer?: AudioBuffer;
  arrayBuffer?: ArrayBuffer;
  setBuffer?: (buffer: AudioBuffer) => void;
  setArrayBuffer?: (buffer: ArrayBuffer) => void;
}

export interface AudioSingleFileUrl {
  id?: number;
  name: string;
  value: string;
  noteTotalDurationInSeconds: number;
  buffer?: AudioBuffer;
  arrayBuffer?: ArrayBuffer;
  setBuffer?: (buffer: AudioBuffer) => void;
  setArrayBuffer?: (buffer: ArrayBuffer) => void;
}

export interface AudioNote {
  id: number;
  index: number;
  name: string;
  isNaturalRelease?: boolean;
}

export interface AudioLib extends BaseAudioLib {
  setContext: (newContext: AudioContext) => void;
  getContext: () => AudioContext;
  loadAudio: () => Promise<void>;
  updateUrlsBuffers: () => Promise<void>;
}

export interface TimeSignature {
  str: string;
  initAtBar?: number;
  getFigure: () => number;
  getBeats: () => number;
}

export interface Tempo {
  value: number;
  timeSignature: TimeSignature;
  initAtBar?: number;
  getBeatDuration: () => number;
  getNoteDuration: (figure: number, dot?: boolean) => number;
}

export const timeSignatureFactory = (str: string): TimeSignature => {
  return {
    str,
    getFigure() {
      return +str.split('/')[1];
    },
    getBeats() {
      return +str.split('/')[0];
    },
  };
};

export const tempoFactory = (value: number, signatureStr: string): Tempo => {
  return {
    value,
    timeSignature: timeSignatureFactory(signatureStr),
    getBeatDuration() {
      // the subdivision when using x/8 time signatures
      // is based on the same factor used by Apple on the Garageband software
      const subdivisions = this.timeSignature.getFigure() === 8 ? 2 : 1;
      const beatDuration = 60 / this.value / subdivisions;

      return beatDuration;
    },
    getNoteDuration(figure: number, dot = false) {
      const noteDuration = this.getBeatDuration() * figure * (dot ? 1.5 : 1);
      const beatUnit = this.timeSignature.getFigure() / 4;

      return noteDuration * beatUnit;
    },
  };
};

export const createContext = () => {
  const ctxClass = window.AudioContext || (window as any).webkitAudioContext;
  let ctx;
  try {
    ctx = new ctxClass();
    // hack for mobile browsers that are starting the context in a suspended state
    // and were not resolving the resume() promise
    ctx.createGain();
  } catch (e) {
    console.log(e);
  }

  return ctx as AudioContext;
};

export const audioLib = observable(<T extends BaseAudioLib>(child: T): T & AudioLib => {
  let context: AudioContext;
  let info: any;
  let tempo: Tempo;

  return {
    setContext(newContext) {
      context = newContext;
      this.context = context;
    },
    getContext() {
      return context;
    },
    loadAudio(): Promise<void> {
      let loaded = 0;
      const p: Promise<void> = new Promise((resolve, reject) => {
        this.urls.forEach((url: AudioUrl) => {
          const xhr = new XMLHttpRequest();
          xhr.open('get', url.value.replace(/#/g, '%23'), true);
          xhr.responseType = 'arraybuffer';
          xhr.onload = () => {
            if (url.setArrayBuffer) {
              url.setArrayBuffer(xhr.response);
            }

            if (loaded === this.urls.length - 1) {
              resolve();
              return;
            }
            loaded += 1;
          };

          xhr.send();
        });
      });

      return p;
    },
    updateUrlsBuffers(): Promise<void> {
      const promise: Promise<void> = new Promise((resolve, reject) => {
        if (!context) {
          reject();
          return;
        }

        let loaded = 0;
        for (const url of this.urls) {
          if (!url.arrayBuffer) {
            reject();
            return;
          }
          if (!url.buffer) {
            context.decodeAudioData(
              url.arrayBuffer,
              buffer => {
                if (url.setBuffer) {
                  url.setBuffer(buffer);
                  if (loaded === this.urls.length - 1) {
                    resolve();
                    return;
                  }
                  loaded += 1;
                } else {
                  reject();
                  return;
                }
              },
              error => {
                console.log(error, url);
                reject();
                return;
              },
            );
          } else {
            resolve();
          }
        }
      });

      return promise;
    },
    ...child,
    urls: child.urls.map(url => ({
      ...url,
      setBuffer(value: AudioBuffer) {
        this.buffer = value;
      },
      setArrayBuffer(buffer) {
        this.arrayBuffer = buffer;
      },
      getBuffer() {
        return this.buffer;
      },
    })),
    init(initTime: number, commands: any) {
      if ((child as any).init) {
        return (child as any).init(initTime);
      }
      if (!context) {
        return;
      }

      if (context.state === 'suspended') {
        context.resume();
      }

      this.urls.forEach(u => {
        if (u.buffer) {
          const track = context.createBufferSource();
          track.loop = true;
          track.buffer = u.buffer;
          const gainNode = context.createGain();
          track.connect(gainNode);
          gainNode.connect(context.destination);
          gainNode.gain.value = commands.gains[u.name].gain;

          const currentTempo = info.tempos.find((t: any) => t.initAtBar <= commands.initAtBar);
          const currentTimeSignature = currentTempo.timeSignature;
          this.setTempo(currentTempo.value, currentTimeSignature.value);

          track.start(initTime, this.getBarInitTime(commands.initAtBar));
          track.loopStart = this.getBarInitTime(commands.initAtBar);
          track.loopEnd = this.getBarInitTime(commands.endAtBar + 1);
        }
      });
    },
    stop() {
      if (!context || context.state === 'closed') {
        return;
      }

      if ((child as any).stop) {
        return (child as any).stop();
      }

      context.close();
      context = undefined as any;
      this.context = undefined as any;
    },
    setTempo(beatsPerminute: number, timeSignatureStr: string) {
      tempo = tempoFactory(beatsPerminute, timeSignatureStr);
    },
    setInfo(value: any) {
      info = value;
    },
    getBarInitTime(barNumber: number, override?: Tempo) {
      tempo = override ?? tempo;
      return tempo.getBeatDuration() * tempo.timeSignature.getBeats() * (barNumber - 1);
    },
  };
});

export const singleFileAudioLib = observable(<T extends BaseAudioLib>(child: T): T & AudioLib => {
  let context: AudioContext;
  let info: any;
  let tempo: Tempo;

  return {
    setContext(newContext) {
      context = newContext;
      this.context = context;
    },
    getContext() {
      return context;
    },
    loadAudio(): Promise<void> {
      const p: Promise<void> = new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('get', this.audioFile.value.replace(/#/g, '%23'), true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = () => {
          if (this.audioFile.setArrayBuffer) {
            this.audioFile.setArrayBuffer(xhr.response);
          }

          resolve();
        };

        xhr.send();
      });

      return p;
    },
    updateUrlsBuffers(): Promise<void> {
      const promise: Promise<void> = new Promise((resolve, reject) => {
        if (!context) {
          reject();
          return;
        }

        if (!this.audioFile.arrayBuffer) {
          reject();
          return;
        }
        if (!this.audioFile.buffer) {
          context.decodeAudioData(
            this.audioFile.arrayBuffer,
            buffer => {
              if (this.audioFile.setBuffer) {
                this.audioFile.setBuffer(buffer);
                resolve();
              } else {
                reject();
                return;
              }
            },
            error => {
              console.log(error, this.audioFile);
              reject();
              return;
            },
          );
        } else {
          resolve();
        }
      });

      return promise;
    },
    ...child,
    audioFile: {
      ...child.audioFile,
      setBuffer(value: AudioBuffer) {
        this.buffer = value;
      },
      setArrayBuffer(buffer) {
        this.arrayBuffer = buffer;
      },
      getBuffer() {
        return this.buffer;
      },
    },
    audioNotes: child.audioNotes,
    stop() {
      if (!context || context.state === 'closed') {
        return;
      }

      if ((child as any).stop) {
        return (child as any).stop();
      }

      context.close();
      context = undefined as any;
      this.context = undefined as any;
    },
    setTempo(beatsPerminute: number, timeSignatureStr: string) {
      tempo = tempoFactory(beatsPerminute, timeSignatureStr);
    },
    setInfo(value: any) {
      info = value;
    },
    getBarInitTime(barNumber: number, override?: Tempo) {
      tempo = override ?? tempo;
      return tempo.getBeatDuration() * tempo.timeSignature.getBeats() * (barNumber - 1);
    },
  };
});
