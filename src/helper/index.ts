export const Commands = {
  LOOP: 'loop',
  AUTHOR: 'author',
  SCORE: 'score',
  SCORE_PAGE: 'score_page',
  INFO: 'info',
  LINK: 'link',
  IMAGE: 'image',
  VIDEO: 'video',
  DESCRIPTION: 'description',
  INSTRUMENT: 'instrument',
  PART: 'part',
  BAR: 'bar',
  REST: 'r',
  CHORD: 'chord',
};

export const csvToArray = (str: string, trimLine = true): string[] =>
  str
    .trim()
    .replace(/\n+?$/g, '')
    .split('\n')
    .map(x => (trimLine ? x.trim() : x));

export const linesToCommands = (lines: string[]): string[][] =>
  lines
    .filter(line => /[0-9]|[a-z]|[A-Z]/.test(line))
    .map(line =>
      line
        .replace(/,+( '')?$/g, '')
        .replace(/, +/g, ',')
        .split(','),
    );

export const cleanString = (str: string): string => str.replace(new RegExp("'", 'g'), '');

/**
 * Transform the detune value in playbackRateValue
 * each semitone has 100 cents and 1 cent is 2 ^ 1/1200, so the ratio between
 * two consecutive notes is 2 ^ (1/12)
 * so, to convert the detune ratio to playback ratio
 * raise the semitoneRatio to the number of semitones power
 * @param {number} v - semitones * 100  (Cents)
 */
export const detuneToPlaybackRateValue = (v: number): number => (2 ** (1 / 12)) ** (v / 100);
