import {
  cleanString,
  csvToArray,
  detuneToPlaybackRateValue,
  linesToCommands,
} from '.';

describe('Helpers', () => {
  it('should clean a string with \' char', () => {
    const str = '\'Testing string\'';

    expect(cleanString(str)).toBe('Testing string');
  });

  it('should transform a csv string to an array', () => {
    const csv = `
      o1, c, 1/4
      o2, c, 1/4
    `;

    expect(csvToArray(csv)).toEqual([
      'o1, c, 1/4',
      'o2, c, 1/4',
    ]);
  });

  it('should transform detune value to playback rate', () => {
    const one = 2 ** (1 / 1200);
    const hundred = 2 ** (1 / 12);

    expect(detuneToPlaybackRateValue(1)).toEqual(one);
    expect(detuneToPlaybackRateValue(100)).toEqual(hundred);
    expect(detuneToPlaybackRateValue(1200)).toBe(2);
  });

  it('should transform a csv line to a command array', () => {
    const lines = [
      'o3, c, 1/4',
      'o3, d, 1/4',
    ];

    expect(linesToCommands(lines)).toEqual([
      ['o3', 'c', '1/4'],
      ['o3', 'd', '1/4'],
    ]);
  });
});
