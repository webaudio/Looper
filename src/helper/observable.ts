interface Observer {
  event: string;
  callback: (...args: any) => void;
}

export interface Observable {
  subscribe: (event: string, callback: (...args: any) => void) => void;
  notify: (event: string, ...args: any) => void;
}

export const observable = <T>(factory: (...args: any) => T) => (...args: any): Observable & T => {
  const observers: Observer[] = [];
  const obj = factory(...args);
  const notify = (event: string, ...args: any) =>
    observers.filter(o => o.event === event).forEach(o => o.callback.apply(obj, [event, ...args]));

  return Object.assign(
    {
      subscribe(event: string, callback: (...args: any) => void) {
        observers.push({
          event,
          callback,
        });
      },
      notify,
    },
    obj,
  );
};
