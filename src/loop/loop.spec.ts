import { createEmptyLoop } from './loop';

describe('Loop', () => {
  it('should create an empty loop object', () => {
    let loop = createEmptyLoop(1);

    expect(loop).toBeDefined();
    expect(loop.id).toBe(1);
    expect(loop.tempo).toBe(60);

    loop = createEmptyLoop();
    expect(loop.id).toBeDefined();
  });
});
