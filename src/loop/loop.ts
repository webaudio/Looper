import { Modifier } from '../modifiers/modifier';

export interface Loop {
  id: number;
  name: string;
  tempo: number;
  timeSignature: string;
  instruments: Instrument[];
  author?: Author;
  score?: Score;
  info: LoopInfo;
}

export interface LoopInfo {
  link?: LoopInfoLink;
  video?: string;
  image?: string;
  description?: string;
}

export interface LoopInfoLink {
  url?: string;
  text?: string;
}

export interface Author {
  id: number;
  name: string;
}

export interface Note {
  toPlay: string | number;
  figure: number;
  rest: boolean;
  command: string[];
  modifiers: Modifier[];
  chord?: number;
  harmonic?: boolean;
  dot?: boolean;
  staccato?: boolean;
  sustainPedal?: boolean;
  gain?: number;
  legatoStart?: () => number;
  legatoStop?: () => number;
  glissandoStart?: () => number;
  glissandoStop?: () => number;
}

export interface Bar {
  id: number;
  isPreLoop: boolean;
  notes: Note[];
}

export interface Part {
  id: number | 'full';
  bars: Bar[];
}

export interface Instrument {
  type: string;
  name: string;
  parts: Part[];
}

export interface Score {
  pages: string[];
}

export const notesNames = [
  { id: 24, name: 'C0' },
  { id: 25, name: 'C#0/Db0' },
  { id: 26, name: 'D0' },
  { id: 27, name: 'D#0/Eb0' },
  { id: 28, name: 'E0' },
  { id: 29, name: 'F0' },
  { id: 30, name: 'F#0/Gb0' },
  { id: 31, name: 'G0' },
  { id: 32, name: 'G#0/Ab0' },
  { id: 33, name: 'A0' },
  { id: 34, name: 'A#0/Bb0' },
  { id: 35, name: 'B0' },
  { id: 36, name: 'C1' },
  { id: 37, name: 'C#1/Db1' },
  { id: 38, name: 'D1' },
  { id: 39, name: 'D#1/Eb1' },
  { id: 40, name: 'E1' },
  { id: 41, name: 'F1' },
  { id: 42, name: 'F#1/Gb1' },
  { id: 43, name: 'G1' },
  { id: 44, name: 'G#1/Ab1' },
  { id: 45, name: 'A1' },
  { id: 46, name: 'A#1/Bb1' },
  { id: 47, name: 'B1' },
  { id: 48, name: 'C2' },
  { id: 49, name: 'C#2/Db2' },
  { id: 50, name: 'D2' },
  { id: 51, name: 'D#2/Eb2' },
  { id: 52, name: 'E2' },
  { id: 53, name: 'F2' },
  { id: 54, name: 'F#2/Gb2' },
  { id: 541, name: 'F#2_harmonic/Gb2_harmonic' },
  { id: 55, name: 'G2' },
  { id: 56, name: 'G#2/Ab2' },
  { id: 57, name: 'A2' },
  { id: 58, name: 'A#2/Bb2' },
  { id: 59, name: 'B2' },
  { id: 591, name: 'B2_harmonic' },
  { id: 60, name: 'C3' },
  { id: 61, name: 'C#3/Db3' },
  { id: 62, name: 'D3' },
  { id: 63, name: 'D#3/Eb3' },
  { id: 631, name: 'D#3_harmonic/Eb3_harmonic' },
  { id: 64, name: 'E3' },
  { id: 641, name: 'E3_harmonic' },
  { id: 65, name: 'F3' },
  { id: 66, name: 'F#3/Gb3' },
  { id: 67, name: 'G3' },
  { id: 68, name: 'G#3/Ab3' },
  { id: 681, name: 'G#3_harmonic/Ab3_harmonic' },
  { id: 69, name: 'A3' },
  { id: 691, name: 'A3_harmonic' },
  { id: 70, name: 'A#3/Bb3' },
  { id: 71, name: 'B3' },
  { id: 711, name: 'B3_harmonic' },
  { id: 72, name: 'C4' },
  { id: 73, name: 'C#4/Db4' },
  { id: 74, name: 'D4' },
  { id: 741, name: 'D4_harmonic' },
  { id: 75, name: 'D#4/Eb4' },
  { id: 751, name: 'D#4_harmonic/Eb4_harmonic' },
  { id: 76, name: 'E4' },
  { id: 761, name: 'E4_harmonic' },
  { id: 77, name: 'F4' },
  { id: 78, name: 'F#4/Gb4' },
  { id: 781, name: 'F#4_harmonic/Gb4_harmonic' },
  { id: 79, name: 'G4' },
  { id: 80, name: 'G#4/Ab4' },
  { id: 81, name: 'A4' },
  { id: 82, name: 'A#4/Bb4' },
  { id: 83, name: 'B4' },
  { id: 84, name: 'C5' },
  { id: 85, name: 'C#5/Db5' },
  { id: 86, name: 'D5' },
  { id: 87, name: 'D#5/Eb5' },
  { id: 88, name: 'E5' },
  { id: 89, name: 'F5' },
  { id: 90, name: 'F#5/Gb5' },
  { id: 91, name: 'G5' },
  { id: 92, name: 'G#5/Ab5' },
  { id: 93, name: 'A5' },
  { id: 94, name: 'A#5/Bb5' },
  { id: 95, name: 'B5' },
  { id: 791, name: 'G4_harmonic' },
  { id: 811, name: 'A4_harmonic' },
  { id: 831, name: 'B4_harmonic' },
  { id: 841, name: 'C5_harmonic' },
  { id: 861, name: 'D5_harmonic' },
  { id: 891, name: 'F5_harmonic' },
  { id: 'ghost_b', name: 'ghost_b' },
  { id: 'ghost_e', name: 'ghost_e' },
  { id: 'ghost_a', name: 'ghost_a' },
  { id: 'ghost_d', name: 'ghost_d' },
  { id: 'ghost_g', name: 'ghost_g' },
];

const getNoteByName = function(name: string): { id: number | string; name: string } {
  const url = notesNames.find(sound => {
    let names = [];
    if (sound.name.includes('#')) {
      names = sound.name.split('/');
    }

    return name === sound.name || (names.length > 0 && (name === names[0] || name === names[1]));
  });

  return url;
};

const getNoteById = function(id: number | string): { id: number | string; name: string } {
  const url = notesNames.find(sound => sound.id === id);

  return url;
};

export const getNote = function(note: Note): { id: number | string; name: string } {
  const id = note.toPlay;
  if (isNaN(+id)) {
    return getNoteByName(id as string);
  }

  return getNoteById(id);
};

export const createEmptyLoop = (id?: number): Loop => {
  return {
    id: id || new Date().getTime(),
    name: '',
    tempo: 60,
    timeSignature: '',
    instruments: [],
    info: {},
  };
};
