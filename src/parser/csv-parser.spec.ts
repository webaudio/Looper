// import { csvToArray, linesToCommands } from '../helper';

// import {
//   csvParser,
//   parseAuthor,
//   parseBar,
//   parseInfoCmd,
//   parseInstrument,
//   parseLoop,
//   parseNote,
//   parsePart,
//   parseRest,
//   parseScoreCmd,
//   parseNoteCommands,
// } from './csv-parser';

// describe('Parser class', () => {
//   const baseCsv = `
//     loop, 1, ts-4/4, bpm-100, 'Test loop'

//       author, 1, 'Fabricio Rodrigues'

//       score
//         score_page, 'score.jpg'
//         score_page, 'score1.jpg'
//       info
//         link, 'http://google.com', 'Google'
//         image, 'image.jpg'
//         video, 'youtubeURL'
//         description, 'Description of the loop'
//       instrument, bass
//         part, 1
//           bar, 1
//             o3, c, 1/4
//             o3, d, 0.25
//             r, 1/4
//   `;
//   const noInfoCsv = `
//     loop, 1, ts-4/4, bpm-100, 'Test loop'

//       author, 1, 'Fabricio Rodrigues'

//       score
//         score_page, 'score.jpg'
//         score_page, 'score1.jpg'
//       instrument, bass
//         part, 1
//           bar, 1
//             o3, c, 1/4
//             o3, d, 0.25
//             r, 1/4
//   `;
//   let lines;
//   let commands;

//   beforeEach(() => {
//     lines = csvToArray(baseCsv);
//     commands = linesToCommands(lines);
//   });

//   it('sanity check', () => {
//     expect(true).toBe(true);
//   });

//   it('should return loop main config', () => {
//     const loop = parseLoop(commands[0]);

//     expect(loop).toBeDefined();
//     expect(loop.id).toEqual(1);
//   });

//   it('should return loop author', () => {
//     const author = parseAuthor(commands[1]);

//     expect(author).toBeDefined();
//     expect(author.name).toEqual('Fabricio Rodrigues');
//   });

//   it('should return loop score', () => {
//     const score = {
//       pages: [parseScoreCmd(commands[3]), parseScoreCmd(commands[4])],
//     };

//     expect(score).toBeDefined();
//     expect(score.pages.length).toBe(2);
//   });

//   it('should return loop info commands', () => {
//     const info = {
//       link: {
//         url: parseInfoCmd(commands[6][1]),
//         text: parseInfoCmd(commands[6][2]),
//       },
//       video: parseInfoCmd(commands[8][1]),
//     };

//     expect(info).toBeDefined();
//     expect(info.link).toEqual({
//       url: 'http://google.com',
//       text: 'Google',
//     });
//     expect(info.video).toEqual('youtubeURL');
//   });

//   it('should return a loop instrument', () => {
//     const instruent = parseInstrument(commands[10]);

//     expect(instruent).toEqual({
//       name: 'bass',
//       parts: [],
//     });
//   });

//   it('should return a loop instrument part', () => {
//     let part = parsePart(commands[11]);

//     expect(part).toEqual({
//       id: 1,
//       bars: [],
//     });

//     part = parsePart(['part', 'full']);
//     expect(part).toEqual({
//       id: 'full',
//       bars: [],
//     });
//   });

//   it('should return a loop bar', () => {
//     const bar = parseBar(commands[12]);

//     expect(bar).toEqual({
//       id: 1,
//       notes: [],
//     });
//   });

//   it('should return a loop note', () => {
//     const note = parseNote(commands[13]);

//     expect(note).toBeDefined();
//     expect(note).toEqual({
//       toPlay: 'C3',
//       figure: 0.25,
//       rest: false,
//       dot: false,
//       harmonic: false,
//       staccato: false,
//       command: ['o3', 'c', '1/4'],
//       modifiers: [],
//     });
//   });

//   it('should return a loop rest', () => {
//     const rest = parseRest(commands[15]);

//     expect(rest).toBeDefined();
//     expect(rest).toEqual({
//       toPlay: '35',
//       figure: 0.25,
//       rest: true,
//       command: ['r', '1/4'],
//       modifiers: [],
//     });
//   });

//   it('should parse the loop', () => {
//     const parsed = csvParser(baseCsv).parse();

//     expect(parsed).toBeDefined();
//     expect(parsed.length).toBe(1);
//     expect(parsed[0].instruments.length).toBe(1);
//   });

//   it('should parse a loop without info', () => {
//     const parsed = csvParser(noInfoCsv).parse();

//     expect(parsed).toBeDefined();
//     expect(parsed.length).toBe(1);
//     expect(parsed[0].info).toEqual({});
//   });

//   it('should parse a note command into modifiers', () => {
//     const commands = [
//       'o3',
//       'c',
//       '1/4',
//       'st',
//       '.',
//       'h',
//       'gnb',
//       'ch1',
//       'ga-50',
//       'pb-100-50',
//       'gi1',
//       'ge1',
//       'li1',
//       'le1',
//       'vb-100-5-forward',
//     ];

//     const modifiers = parseNoteCommands(commands.slice(3));

//     expect(modifiers.length).toBe(12);
//     expect(modifiers[0].type).toBe('staccato');
//     expect(modifiers[1].type).toBe('dot');
//   });
// });
