import { Commands, cleanString, csvToArray, linesToCommands } from '../helper';
import { Loop, createEmptyLoop, Author, Instrument, Note, Bar, Part, getNote, notesNames } from '../loop/loop';
import { Modifier } from '../modifiers/modifier';
import { glissando } from '../modifiers/glissando';
import { legato } from '../modifiers/legato';
import { pitchBend } from '../modifiers/pitch-bend';
import { vibrato } from '../modifiers/vibrato';
import { getChordShapeSemitones } from '../theory/chords';

export const parseLoop = (command: string[]): Loop => {
  const loop: Loop = createEmptyLoop(+command[1]);

  command.slice(2).forEach((prop: string) => {
    if (/ts-[0-9]+\/[0-9]+/g.test(prop)) {
      loop.timeSignature = prop.replace('ts-', '');
    }
    if (/bpm-[0-9]+/g.test(prop)) {
      loop.tempo = +prop.replace('bpm-', '');
    }
    if (prop.includes("'")) {
      loop.name = cleanString(prop);
    }
  });

  return loop;
};

export const parseAuthor = (command: string[]): Author => {
  const author = {
    id: +command[1],
    name: cleanString(command[2]),
  };

  return author;
};

export const parseScoreCmd = (command: string[]): string => cleanString(command[1]);

export const parseInfoCmd = (command: string): string => cleanString(command);

export const parseInstrument = (command: string[]): Instrument => ({
  type: command[1],
  name: command[2] ?? command[1],
  parts: [],
});

export const parsePart = (command: string[]): Part => ({
  id: +command[1] || 'full',
  bars: [],
});

export const parseBar = (command: string[]): Bar => ({
  id: +command[1],
  isPreLoop: command.includes('preLoop'),
  notes: [],
});

const strToFigure = (str: string): number => {
  const figure = str.includes('/') ? str.split('/').reduce((p, c, i) => (i === 1 ? p / +c : p * +c), 1) : +str;

  return figure;
};

/**
 *  note Commands
 *    li[0-9]+: legato init (legatoStart)
 *    le[0-9]+: legato end (legatoStop)
 *    gi[0-9]+: glissando init (glissandoStart)
 *    ge[0-9]+: glissando end (glissandoStop)
 *    pb-([0-9]+)-([0-9]+): pitch bend (amount, pct)
 *    gn[b,e,a,d,g]: ghost note (ghost_note_[b,e,a,d,g])
 *    ch[0-9]+: chord
 *    st: staccato
 *    sp: sustain pedal
 *    . or dt: dot
 *    h: harmonic
 *    ga-[0-9]: gain (0-100)
 *    vb-([0-9])+-([0-9])+-(\w)+: vibrato
 */
export const parseNoteCommands = (commands: string[], note: Note): Note => {
  const modifiers: Modifier[] = [];
  const newNote = note;

  commands.forEach(command => {
    // Staccato
    if (command === 'st') {
      newNote.staccato = true;

      return;
    }

    // Sustain Pedal
    if (command === 'sp') {
      newNote.sustainPedal = true;

      return;
    }

    // dot
    if (command === '.' || command === 'dt') {
      newNote.dot = true;

      return;
    }

    // Harmonic
    if (command === 'h') {
      newNote.harmonic = true;
      newNote.toPlay += '_harmonic';

      return;
    }

    // Ghost note
    if (/gn[beadg]+/.test(command)) {
      newNote.toPlay = 'ghost_' + command.replace('gn', '');

      return;
    }

    // Chord
    if (/ch[0-9]+/.test(command)) {
      newNote.chord = +command.replace('ch', '');
      return;
    }

    // Gain
    if (/ga-[0-9]+/.test(command)) {
      newNote.gain = +command.replace('ga-', '') / 100;

      return;
    }

    // Pitch Bend
    if (/pb-([0-9])+-([0-9])+/.test(command)) {
      const aux = command.trim().split('-');
      modifiers.push(
        pitchBend({
          amount: +aux[1],
          percent: +aux[2] / 100,
        }),
      );

      return;
    }

    // Glissando
    if (/gi[0-9]+/.test(command)) {
      modifiers.push(
        glissando({
          start: command.replace('gi', ''),
        }),
      );
      return;
    } else if (/ge[0-9]+/.test(command)) {
      modifiers.push(
        glissando({
          stop: command.replace('ge', ''),
        }),
      );
      return;
    }

    // Legato
    if (/li[0-9]+/.test(command)) {
      modifiers.push(
        legato({
          start: command.replace('li', ''),
        }),
      );

      return;
    } else if (/le[0-9]+/.test(command)) {
      modifiers.push(
        legato({
          stop: command.replace('le', ''),
        }),
      );
      return;
    }

    // Vibrato
    if (/vb-([0-9])+-([0-9])+-(\w)+/.test(command)) {
      const aux = command.trim().split('-');
      modifiers.push(
        vibrato({
          amount: +aux[1],
          divisions: +aux[2],
          direction: aux[3],
        }),
      );

      return;
    }
  });

  return {
    ...newNote,
    modifiers,
  };
};

/**
 * A note can be a number or a name (C, F#, G, hh-closed, kick, snare)
 *
 * If the first command is nn (note-name), the note is octave independent
 * That is useful for instruments like drums, percussion...
 * @param command string[] - [octave|note-name, name|number]
 * @returns Note
 */
export const parseNote = (command: string[]): Note => {
  const toPlay = command[0].includes('o')
    ? command[1].charAt(0).toUpperCase() + command[1].slice(1) + command[0].replace('o', '')
    : command[1].toLocaleLowerCase();
  const figure = strToFigure(command[2]);
  const note: Note = {
    toPlay: isNaN(+toPlay) ? toPlay : +toPlay,
    figure,
    rest: false,
    harmonic: false,
    dot: false,
    staccato: false,
    modifiers: null,
    command,
    legatoStart() {
      const modifier = this.modifiers.find(m => m.type === 'legato' && !!m.start);

      return modifier?.start;
    },
    legatoStop() {
      const modifier = this.modifiers.find(m => m.type === 'legato' && !!m.stop);

      return modifier?.stop;
    },
    glissandoStart() {
      const modifier = this.modifiers.find(m => m.type === 'glissando' && !!m.start);

      return modifier?.start;
    },
    glissandoStop() {
      const modifier = this.modifiers.find(m => m.type === 'glissando' && !!m.stop);

      return modifier?.stop;
    },
  };

  return parseNoteCommands(command.slice(2), note);
};

export const parseRest = (command: string[]): Note => {
  const note = {
    toPlay: '35',
    figure: strToFigure(command[1]),
    rest: true,
    command,
    modifiers: [],
    legatoStop() {
      return 0;
    },
    glissandoStop() {
      return 0;
    },
  };

  return parseNoteCommands(command.slice(2), note);
};

export const transposeNote = (line: string, semitones) => {
  const command = linesToCommands([line.trim()])[0];
  const parsed = parseNote(command);
  const note = getNote(parsed);

  // TODO - Validate transposition
  if (isNaN(+note.id) || note.name.includes('ghost') || note.name.includes('harmonic')) {
    return line;
  }

  const transposed = getNote({ ...parsed, toPlay: (note.id as number) + semitones });
  if (!transposed) {
    throw new Error(`Could not transpose the note ${note.name} in ${semitones} semitone(s)`);
  }
  const octave = +transposed.name.split('/')[0].replace(/[a-z]+#?/gi, '');
  const name = transposed.name.split('/').map(n => n.replace(/\d/g, ''))[0];

  return line.replace(/^o(\d)/, `o${octave}`).replace(/,\s?[a-z]+#?\s?,/i, `, ${name.toLocaleLowerCase()},`);
};

export const parseChord = (command: string[], chordIndex: number): Note[] => {
  const [, octave, noteName, slug, inversion, ...noteCommandsInChord] = command;
  let noteCommands = [octave, noteName, ...noteCommandsInChord, `ch${chordIndex}`];
  const shape = getChordShapeSemitones(slug, +inversion);

  const legatoStart = noteCommands.find(c => /^li\d+/.test(c));
  const legatoEnd = noteCommands.find(c => /^le\d+/.test(c));

  const notes = shape.map((semitones, index) => {
    if (legatoStart || legatoEnd) {
      noteCommands = noteCommands.map(c => {
        if (/^li\d+/.test(c)) {
          return `li${+c.replace('li', '') + index}`;
        } else if (/^le\d+/.test(c)) {
          return `le${+c.replace('le', '') + index}`;
        }

        return c;
      });
    }

    const note = parseNote(linesToCommands([transposeNote(noteCommands.join(', '), semitones)])[0]);

    return note;
  });

  return notes;
};

export interface Parser {
  parse: () => Loop[];
  transpose: (semitones: number) => string;
}

export const csvParser: (csvContent: string) => Parser = (csvContent: string) => {
  return {
    parse(): Loop[] {
      let parsed: Loop[] = [];
      let loop: Loop;
      let instrument: Instrument;
      let part: Part;
      let bar: Bar;
      let note: Note;
      let chordIndex = 1;

      const commands: string[][] = linesToCommands(csvToArray(csvContent));
      commands.forEach(command => {
        switch (command[0]) {
          case Commands.LOOP:
            loop = parseLoop(command);
            parsed = [...parsed, loop];
            break;
          case Commands.AUTHOR:
            loop.author = parseAuthor(command);
            break;
          case Commands.SCORE:
            loop.score = {
              pages: [],
            };
            break;
          case Commands.SCORE_PAGE:
            loop.score && loop.score.pages.push(parseScoreCmd(command));
            break;
          case Commands.INFO:
            loop.info = {};
            break;
          case Commands.LINK:
            loop.info.link = {
              url: parseInfoCmd(command[1]),
              text: parseInfoCmd(command[2]),
            };
            break;
          case Commands.VIDEO:
            loop.info.video = parseInfoCmd(command[1]);
            break;
          case Commands.IMAGE:
            loop.info.image = parseInfoCmd(command[1]);
            break;
          case Commands.DESCRIPTION:
            loop.info.description = parseInfoCmd(command[1]);
            break;
          case Commands.INSTRUMENT:
            instrument = parseInstrument(command);
            loop.instruments.push(instrument);
            break;
          case Commands.PART:
            part = parsePart(command);
            instrument.parts.push(part);
            break;
          case Commands.BAR:
            bar = parseBar(command);
            part.bars.push(bar);
            break;
          case Commands.REST:
            note = parseRest(command);
            bar.notes.push(note);
            break;
          case Commands.CHORD:
            bar.notes = [...bar.notes, ...parseChord(command, chordIndex)];
            chordIndex += 1;
            break;
          default:
            note = parseNote(command);
            bar.notes.push(note);
        }
      });

      return parsed;
    },

    transpose(semitones: number): string {
      const lines = csvToArray(csvContent, false);

      return lines
        .map(line => {
          if (!/^o(\d),/.test(line.trim())) {
            return line;
          }

          const command = linesToCommands([line.trim()])[0];
          const parsed = parseNote(command);
          const note = getNote(parsed);

          // TODO - Validate transposition
          if (isNaN(+note.id) || note.name.includes('ghost') || note.name.includes('harmonic')) {
            return line;
          }

          const transposed = getNote({ ...parsed, toPlay: (note.id as number) + semitones });
          if (!transposed) {
            throw new Error(`Could not transpose the note ${note.name} in ${semitones} semitone(s)`);
          }
          const octave = +transposed.name.split('/')[0].replace(/[a-z]+#?/gi, '');
          const name = transposed.name.split('/').map(n => n.replace(/\d/g, ''))[0];

          return line.replace(/^o(\d)/, `o${octave}`).replace(/,\s?[a-z]+#?\s?,/i, `, ${name.toLocaleLowerCase()},`);
        })
        .join('\n');
    },
  };
};
