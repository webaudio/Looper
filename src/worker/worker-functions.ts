export const createWorker = (ctx: any) => {
  let intervalId: number;
  const tick = () => {
    ctx.postMessage({ type: 'tick' });
  };

  const ticker = () => {
    intervalId = setInterval(() => {
      tick();
    }, 15);
  };

  ctx.addEventListener('message', event => {
    switch (event.data.type) {
      case 'stop':
        clearInterval(intervalId);
        break;
      case 'schedule':
        setTimeout(() => {
          ctx.postMessage({
            type: event.data.eventName,
            payload: event.data.payload,
          });
        }, event.data.time);
        break;
      default:
        ticker();
    }
  });
};
