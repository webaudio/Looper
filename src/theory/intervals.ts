export const INTERVALS = [
  {
    name: 'Perfect first',
    slug: 'p1',
    semitones: 0,
  },
  {
    name: 'Minor second',
    slug: '2m',
    semitones: 1,
  },
  {
    name: 'Major second',
    slug: '2M',
    semitones: 2,
  },
  {
    name: 'Minor third',
    slug: '3m',
    semitones: 3,
  },
  {
    name: 'Major third',
    slug: '3M',
    semitones: 4,
  },
  {
    name: 'Perfect fourth',
    slug: '4J',
    semitones: 5,
  },
  {
    name: 'Augmented fourth',
    slug: '4aum',
    semitones: 6,
  },
  {
    name: 'Diminished fifth',
    slug: '5dim',
    semitones: 6,
  },
  {
    name: 'Perfect Fifth',
    slug: '5J',
    semitones: 7,
  },
  {
    name: 'Augmented Fifth',
    slug: '5aum',
    semitones: 8,
  },
  {
    name: 'Minor Sixth',
    slug: '6m',
    semitones: 8,
  },
  {
    name: 'Major Sixth',
    slug: '6M',
    semitones: 9,
  },
  {
    name: 'Diminished Seventh',
    slug: '7dim',
    semitones: 9,
  },
  {
    name: 'Minor Seventh',
    slug: '7',
    semitones: 10,
  },
  {
    name: 'Major Seventh',
    slug: '7M',
    semitones: 11,
  },
  {
    name: 'Major Seventh',
    slug: '7M',
    semitones: 11,
  },
];
