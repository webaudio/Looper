// command, octave, note, slug, inversion(0-3), figure
// chord, o3, c, m7, 0, 1/2

export const pianoChordShapes = [
  {
    slug: 'maj7',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 4, 7, 11],
      },
      {
        inversion: 1,
        intervals: [-8, -5, -1, 0],
      },
      {
        inversion: 2,
        intervals: [-5, -1, 0, 4],
      },
      {
        inversion: 3,
        intervals: [-1, 0, 4, 7],
      },
    ],
  },
  {
    slug: 'maj7(#11)',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 4, 6, 11],
      },
      {
        inversion: 1,
        intervals: [-8, -6, -1, 0],
      },
      {
        inversion: 2,
        intervals: [-6, -1, 0, 4],
      },
      {
        inversion: 3,
        intervals: [-1, 0, 4, 6],
      },
    ],
  },
  {
    slug: '6/9',
    shapes: [
      {
        inversion: 0,
        intervals: [4, 9, 14],
      },
      {
        inversion: 1,
        intervals: [-3, 2, 4],
      },
      {
        inversion: 2,
        intervals: [-8, -3, 2],
      },
    ],
  },
  {
    slug: 'm7',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 3, 7, 10],
      },
      {
        inversion: 1,
        intervals: [-9, -5, -2, 0],
      },
      {
        inversion: 2,
        intervals: [-5, -2, 0, 3],
      },
      {
        inversion: 3,
        intervals: [-2, 0, 3, 7],
      },
    ],
  },
  {
    slug: 'm(7M)',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 3, 7, 11],
      },
      {
        inversion: 1,
        intervals: [-9, -5, -1, 0],
      },
      {
        inversion: 2,
        intervals: [-5, -1, 0, 3],
      },
      {
        inversion: 3,
        intervals: [-1, 0, 3, 7],
      },
    ],
  },
  {
    slug: 'm6',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 3, 7, 9],
      },
      {
        inversion: 1,
        intervals: [-9, -5, -3, 0],
      },
      {
        inversion: 2,
        intervals: [-5, -3, 0, 3],
      },
      {
        inversion: 3,
        intervals: [-3, 0, 3, 7],
      },
    ],
  },
  {
    slug: 'm7(b5)',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 3, 6, 10],
      },
      {
        inversion: 1,
        intervals: [-9, -6, -2, 0],
      },
      {
        inversion: 2,
        intervals: [-6, -2, 0, 3],
      },
      {
        inversion: 3,
        intervals: [-2, 0, 3, 6],
      },
    ],
  },
  {
    slug: 'dim',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 3, 6, 9],
      },
      {
        inversion: 1,
        intervals: [-9, -6, -3, 0],
      },
      {
        inversion: 2,
        intervals: [-6, -3, 0, 3],
      },
      {
        inversion: 3,
        intervals: [-3, 0, 3, 6],
      },
    ],
  },
  {
    slug: '7',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 4, 7, 10],
      },
      {
        inversion: 1,
        intervals: [-8, -5, -2, 0],
      },
      {
        inversion: 2,
        intervals: [-5, -2, 0, 4],
      },
      {
        inversion: 3,
        intervals: [-2, 0, 4, 7],
      },
    ],
  },
  {
    slug: '7(b9)',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 4, 7, 10, 13],
      },
      {
        inversion: 1,
        intervals: [-8, -5, -2, 1],
      },
      {
        inversion: 2,
        intervals: [-5, -2, 1, 4],
      },
      {
        inversion: 3,
        intervals: [-2, 1, 4, 7],
      },
    ],
  },
  {
    slug: '7(9)',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 4, 7, 10, 14],
      },
      {
        inversion: 1,
        intervals: [-8, -5, -2, 2],
      },
      {
        inversion: 2,
        intervals: [-5, -2, 2, 4],
      },
      {
        inversion: 3,
        intervals: [-2, 2, 4, 7],
      },
    ],
  },
  {
    slug: '7(#9)',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 4, 7, 10, 15],
      },
      {
        inversion: 1,
        intervals: [-8, -5, -2, 3],
      },
      {
        inversion: 2,
        intervals: [-5, -2, 3, 4],
      },
      {
        inversion: 3,
        intervals: [-2, 3, 4, 7],
      },
    ],
  },
  {
    slug: '7(#11)',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 4, 6, 10],
      },
      {
        inversion: 1,
        intervals: [-8, -6, -2, 0],
      },
      {
        inversion: 2,
        intervals: [-6, -2, 0, 4],
      },
      {
        inversion: 3,
        intervals: [-2, 0, 4, 6],
      },
    ],
  },
  {
    slug: '7(b13)',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 4, 8, 10],
      },
      {
        inversion: 1,
        intervals: [-8, -4, -2, 0],
      },
      {
        inversion: 2,
        intervals: [-4, -2, 0, 4],
      },
      {
        inversion: 3,
        intervals: [-2, 0, 4, 8],
      },
    ],
  },
  {
    slug: '7(13)',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 4, 9, 10],
      },
      {
        inversion: 1,
        intervals: [-8, -3, -2, 0],
      },
      {
        inversion: 2,
        intervals: [-3, -2, 0, 4],
      },
      {
        inversion: 3,
        intervals: [-2, 0, 4, 9],
      },
    ],
  },
  {
    slug: 'sus',
    shapes: [
      {
        inversion: 0,
        intervals: [0, 5, 10, 14],
      },
      {
        inversion: 1,
        intervals: [-7, -2, 0, 2],
      },
      {
        inversion: 2,
        intervals: [-2, 0, 2, 5],
      },
      {
        inversion: 3,
        intervals: [-10, -7, -2, 0],
      },
    ],
  },
];

export const getChordShapeSemitones = (slug: string, inversion: number): number[] => {
  return pianoChordShapes.find(chord => chord.slug === slug)?.shapes[inversion]?.intervals;
};
