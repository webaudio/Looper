export type ApplyFunction = (params: ModifierParam) => void;
export type DetuneFactory = (params: ModifierParam, usePlaybackRate?: boolean) => void;
export type DetuneFunction = (params: ModifierParam, usePlaybackRate?: boolean) => void;

export interface Detune {
  value: number;
  duration: number;
}

export interface Modifier {
  type: string;
  apply?: ApplyFunction;
  start?: number;
  stop?: number;
  id?: number;
}

export interface ModifierParam {
  note: any;
  bars: any[];
  barInfo: any;
  tempo: any;
  source: AudioBufferSourceNode;
  currentTime: number;
}

export const createApply = (applyFn: ApplyFunction) => (params: ModifierParam): void => applyFn(params);
export const createDetune = (detune: DetuneFunction, apply: ApplyFunction) => (
  params: ModifierParam,
  usePlaybackRate = false,
): void => {
  apply(params);
  return detune(params, usePlaybackRate);
};
