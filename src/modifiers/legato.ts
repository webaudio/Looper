import { detuneToPlaybackRateValue } from '../helper';
import { getNote } from '../loop/loop';
import {
  Detune,
  Modifier,
  createApply,
  createDetune,
  ModifierParam,
  DetuneFunction,
  ApplyFunction,
  DetuneFactory,
} from './modifier';

interface Legato extends Modifier {
  detunes: Detune[];
  detune: DetuneFactory;
}

export const legato = (config: any): Legato => {
  const start = +config.start;
  const stop = +config.stop;
  let duration = 0;
  let newBarInfo;
  let detunesList: Detune[] = [];

  const applyFn: ApplyFunction = (params: ModifierParam) => {
    duration = 0;
    detunesList = [];
    const firstNote = params.note;
    const recursive = ({ note, bars, barInfo, tempo, source, currentTime }: ModifierParam): void => {
      let { index: barIndex, noteIndex } = barInfo;
      if (!note || !note.legatoStart()) {
        newBarInfo = {
          index: barIndex,
          noteIndex,
        };

        return;
      }

      noteIndex += 1;
      if (!bars[barIndex - 1]?.notes[noteIndex]) {
        noteIndex = 0;
        barIndex += 1;
        if (!bars[barIndex - 1]) {
          barIndex = bars.filter(b => b.isPreLoop).length + 1;
        }
      }

      let nextNote = bars[barIndex - 1]?.notes[noteIndex];

      if (note.toPlay != nextNote.toPlay) {
        while (!nextNote || !nextNote.legatoStop() || nextNote.legatoStop() !== note.legatoStart()) {
          noteIndex++;
          if (!bars[barIndex - 1].notes[noteIndex]) {
            noteIndex = 0;
            barIndex += 1;
          }

          nextNote = bars[barIndex - 1]?.notes[noteIndex];
        }
      }

      const durationNextNote = tempo.getNoteDuration(nextNote.figure, nextNote.dot);

      duration += durationNextNote;

      const noteNumber = getNote(firstNote).id as number;
      const nextNoteNumber = getNote(nextNote).id as number;

      detunesList.push({
        duration: tempo.getNoteDuration(note.figure, note.dot),
        value: 100 * (nextNoteNumber - noteNumber),
      });

      recursive({
        note: nextNote,
        bars,
        barInfo: { index: barIndex, noteIndex },
        tempo,
        source,
        currentTime,
      });
    };

    recursive(params);
  };

  const detuneFn: DetuneFunction = ({ source, currentTime, barInfo }: ModifierParam, usePlaybackRate?: boolean) => {
    let timeSum = 0;

    detunesList.forEach(detune => {
      let detuneValue = detune.value;
      if (!source.detune) {
        detuneValue = detuneToPlaybackRateValue(detuneValue);
        source.playbackRate.setTargetAtTime(detuneValue, currentTime + detune.duration + timeSum, 0.001);
      } else {
        source.detune.setTargetAtTime(detuneValue, currentTime + detune.duration + timeSum, 0.001);
      }

      timeSum += detune.duration;
    });

    return { duration, barInfo };
  };
  return {
    type: 'legato',
    start,
    stop,
    get detunes(): Detune[] {
      return detunesList;
    },
    detune: createDetune(detuneFn, createApply(applyFn)),
  };
};
