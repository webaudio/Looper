import { detuneToPlaybackRateValue } from '../helper';
import {
  Detune,
  Modifier,
  createApply,
  createDetune,
  ModifierParam,
  DetuneFunction,
  ApplyFunction,
  DetuneFactory,
} from './modifier';

interface PitchBend extends Modifier {
  amount: number;
  percentToInit: number;
  detunes: Detune[];
  detune: DetuneFactory;
}

export const pitchBend = (config: any): PitchBend => {
  const amount = config.amount;
  const percentToInit = config.percent;
  let duration = 0;
  let newBarInfo;
  let detunesList: Detune[] = [];

  const applyFn: ApplyFunction = ({ note, barInfo, tempo }: ModifierParam) => {
    newBarInfo = barInfo;
    duration = tempo.getNoteDuration(note.figure, note.dot);
    detunesList = [
      {
        duration,
        value: amount,
      },
    ];
  };

  const detuneFn: DetuneFunction = ({ source, currentTime }: ModifierParam, usePlaybackRate?: boolean) => {
    detunesList.forEach(detune => {
      let detuneValue = detune.value;
      if (!source.detune) {
        detuneValue = detuneToPlaybackRateValue(detuneValue);
        source.playbackRate.setTargetAtTime(detuneValue, currentTime, percentToInit * detune.duration);
      } else {
        source.detune.setTargetAtTime(detuneValue, currentTime, percentToInit * detune.duration);
      }
    });

    return { duration, barInfo: newBarInfo };
  };
  return {
    type: 'pitchBend',
    amount,
    percentToInit,
    get detunes(): Detune[] {
      return detunesList;
    },
    detune: createDetune(detuneFn, createApply(applyFn)),
  };
};
