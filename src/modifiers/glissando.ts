import { detuneToPlaybackRateValue } from '../helper';
import { getNote } from '../loop/loop';
import {
  Detune,
  Modifier,
  createApply,
  createDetune,
  ModifierParam,
  DetuneFunction,
  ApplyFunction,
  DetuneFactory,
} from './modifier';

interface Glissando extends Modifier {
  detunes: Detune[];
  detune: DetuneFactory;
}

export const glissando = (config: any): Glissando => {
  const start = +config.start;
  const stop = +config.stop;
  let duration = 0;
  let newBarInfo;
  let detunesList: Detune[] = [];

  const applyFn: ApplyFunction = (params: ModifierParam) => {
    duration = 0;
    detunesList = [];
    const recursive = ({ note, bars, barInfo, tempo, source, currentTime }: ModifierParam): void => {
      let { index: barIndex, noteIndex } = barInfo;
      if (!note || !note.glissandoStart()) {
        newBarInfo = {
          index: barIndex,
          noteIndex,
        };

        return;
      }

      noteIndex += 1;
      if (!bars[barIndex - 1]?.notes[noteIndex]) {
        noteIndex = 0;
        barIndex += 1;
      }

      let nextNote = bars[barIndex - 1]?.notes[noteIndex];

      while (!nextNote || !nextNote.glissandoStop() || nextNote.glissandoStop() !== note.glissandoStart()) {
        noteIndex++;
        if (!bars[barIndex - 1].notes[noteIndex]) {
          noteIndex = 0;
          barIndex += 1;
        }

        nextNote = bars[barIndex - 1]?.notes[noteIndex];
      }

      const durationNextNote = tempo.getNoteDuration(nextNote.figure, nextNote.dot);

      duration += durationNextNote;

      const noteNumber = getNote(note).id as number;
      const nextNoteNumber = getNote(nextNote).id as number;

      detunesList.push({
        duration: tempo.getNoteDuration(note.figure, note.dot),
        value: 100 * (nextNoteNumber - noteNumber),
      });

      recursive({
        note: nextNote,
        bars,
        barInfo: { index: barIndex, noteIndex },
        tempo,
        source,
        currentTime,
      });
    };

    recursive(params);
  };

  const detuneFn: DetuneFunction = ({ source, currentTime }: ModifierParam, usePlaybackRate?: boolean) => {
    let timeSum = 0;

    detunesList.forEach(detune => {
      let detuneValue = detune.value;
      if (!source.detune) {
        detuneValue = detuneToPlaybackRateValue(detuneValue);
        source.playbackRate.setTargetAtTime(detuneValue, currentTime + detune.duration + timeSum, detune.duration);
      } else {
        source.detune.setTargetAtTime(detuneValue, currentTime + detune.duration + timeSum, detune.duration);
      }
      timeSum += detune.duration;
    });

    return { duration, barInfo: newBarInfo };
  };
  return {
    type: 'glissando',
    start,
    stop,
    get detunes(): Detune[] {
      return detunesList;
    },
    detune: createDetune(detuneFn, createApply(applyFn)),
  };
};
