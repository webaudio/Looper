// import { createApply, createDetune } from './modifier';

// describe('Modufier', () => {
//   it('sanity check', () => {
//     expect(true).toBe(true);
//   });

//   it('should return a new apply function', () => {
//     const config = {
//       note: {},
//     };

//     const apply = createApply(config, c => c);

//     expect(apply()).toEqual(config);
//   });

//   it('should return a new detune function', () => {
//     const detune = (): unknown => [
//       {
//         time: 0,
//         duration: 10,
//         value: 10,
//       },
//     ];

//     const detuneFn = createDetune(detune);

//     const config = {
//       note: {},
//     };

//     const apply = createApply(config, c => c);

//     expect(detuneFn).toBeDefined();
//     expect(detuneFn(apply)).toEqual(detune());
//   });

//   // it('should return a new modifier', () => {
//   //   const mod = modifier('mod', {
//   //     apply: createApply({ note: {} }, c => c),
//   //     detune: createDetune(() => [
//   //       {
//   //         time: 0,
//   //         duration: 10,
//   //         value: 10,
//   //       },
//   //     ]),
//   //   });

//   //   expect(mod).toBeDefined();
//   // });
// });
