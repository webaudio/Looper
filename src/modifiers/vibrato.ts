import { detuneToPlaybackRateValue } from '../helper';
import {
  Detune,
  Modifier,
  createApply,
  createDetune,
  ModifierParam,
  DetuneFunction,
  ApplyFunction,
  DetuneFactory,
} from './modifier';

interface Vibrato extends Modifier {
  detunes: Detune[];
  detune: DetuneFactory;
}

export const vibrato = (config: any): Vibrato => {
  const amount = config.amount;
  const divisions = config.divisions;
  const direction = config.direction;
  let oscilation = direction === 'duplex' ? -amount : 0;
  let duration = 0;
  let newBarInfo;
  let detunesList: Detune[] = [];

  const applyFn: ApplyFunction = ({ note, tempo, barInfo }: ModifierParam) => {
    newBarInfo = barInfo;
    duration = 0;
    detunesList = [];

    const partDuration = tempo.getNoteDuration(note.figure, note.dot) / divisions / 2;

    for (let i = 0; i < divisions; i++) {
      detunesList.push({
        duration: partDuration,
        value: amount,
      });
    }
  };

  const detuneFn: DetuneFunction = ({ source, currentTime }: ModifierParam, usePlaybackRate?: boolean) => {
    let startTime = currentTime;
    detunesList.forEach(detune => {
      let detuneValue = detune.value;
      if (!source.detune) {
        detuneValue = detuneToPlaybackRateValue(detuneValue);
        oscilation = detuneToPlaybackRateValue(oscilation);
        source.playbackRate.setTargetAtTime(detuneValue, startTime, detune.duration);
        source.playbackRate.setTargetAtTime(oscilation, startTime + detune.duration, detune.duration);
      } else {
        source.detune.setTargetAtTime(detuneValue, startTime, detune.duration);
        source.detune.setTargetAtTime(oscilation, startTime + detune.duration, detune.duration);
      }
      startTime += detune.duration * 2;
    });

    return { duration, barInfo: newBarInfo };
  };
  return {
    type: 'vibrato',
    get detunes(): Detune[] {
      return detunesList;
    },
    detune: createDetune(detuneFn, createApply(applyFn)),
  };
};
