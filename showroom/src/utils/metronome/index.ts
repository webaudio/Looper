import MetronomeWorker from 'worker-loader!./metronome-worker.ts';
import { metronome } from '@fabriciomendonca/webaudio-looper'

export const createMetronome = () => {
  const worker = new MetronomeWorker();

  return metronome(worker);
};
