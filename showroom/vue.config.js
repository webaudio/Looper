module.exports = {
  configureWebpack: {
    devtool: 'source-map',
  },
  chainWebpack: config => {
    config.plugin('html').tap(args => {
      args[0].title = 'Bass Practice Club';
      return args;
    });

    config.module
      .rule('worker')
      .test(/\.worker\.ts$/)
      .use('worker-loader')
      .loader('worker-loader');
  },
};
